# Adms

This project was generated with [Angular CLI] version 6.2.3.

## DEMO

LINK: https://test001-lizhang.firebaseapp.com

![alt text](https://firebasestorage.googleapis.com/v0/b/test001-lizhang.appspot.com/o/demo1.jpg?alt=media&token=09da29a1-86a4-40f2-b79f-51aae9ba3b65)

![alt text](https://firebasestorage.googleapis.com/v0/b/test001-lizhang.appspot.com/o/demo2.jpg?alt=media&token=26f95f8a-b1a2-490b-8248-8fbe5a881276)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
