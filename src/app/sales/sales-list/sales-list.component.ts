import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesService } from '../../shared/sales.service';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogConfig } from '@angular/material';
import { DepartmentService } from 'src/app/shared/department.service';
import { NotificationService } from 'src/app/shared/notification.service';
import { SalesDetailComponent } from '../sales-detail/sales-detail.component';
import { CarService } from 'src/app/shared/car.service';
import { CustomerService } from 'src/app/shared/customer.service';

@Component({
  selector: 'app-sales-list',
  templateUrl: './sales-list.component.html',
  styleUrls: ['./sales-list.component.css']
})
export class SalesListComponent implements OnInit {

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['car', 'customers', 'detail', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;

  // customerArray: MatTableDataSource<any>;

  // carDetails = [];
  // userDetails = [];

  constructor(
    private router: Router,
    private service: SalesService,
    private customerService: CustomerService,
    private dialog: MatDialog,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    //  let customerIds = '';
    const that = this;
    let detail = [];
    let array = null;
    let customerIds = null;
    let nameArray = null;
    let cust = null;
    let final = null;
    this.service.getSales().subscribe(
      list => {
        array = list.map(item => {
       //  console.log((item.payload.val())['customers']);
         customerIds = (item.payload.val())['customers'];
       // console.log(customerIds);
         nameArray = customerIds.split(',');
        detail = [];
        nameArray.forEach (
          (name) => {
          //  console.log(name);
             cust = that.customerService.getCustomerDetails(name);
             final =
                    name + ' : ' +
                    cust.fullName + ' ' +
                    cust.email + ' ' +
                    cust.mobile + ' ' +
                    cust.city + ' ' +
                    cust.gender + ' ' +
                    cust. department + ' ' +
                    cust.hireDate + ' ' +
                    cust.isPermanent + ' || ';
            detail.push(final);
          } );

      //    console.log(detail);

         return {
           $key: item.key,
           detail: detail,
           ...item.payload.val()
         };
       });
       this.listData = new MatTableDataSource(array);
       this.listData.sort = this.sort;
       this.listData.paginator = this.paginator;
       this.listData.filterPredicate = (data, filter) => {
         return this.displayedColumns.some(ele => {
           return ele !== 'actions' && ele !== 'detail' && data[ele].toLowerCase().indexOf(filter) !== -1;
         });
       };
     });
  }

  onSearchClear() {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLocaleLowerCase();
  }

  onCreate () {
    this.service.initializeFormGroup();
    this.router.navigate(['/sale/new']);

    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.width = '60%';
    // this.dialog.open(SalesDetailComponent, dialogConfig);
  }


  onEdit(row) {
    this.service.populateForm(row);
    this.router.navigate(['/sale/' + row.$key]);

    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.width = '60%';
    // this.dialog.open(SalesDetailComponent, dialogConfig);
  }

  onDelete($key) {
    if (confirm('Are you sure to delete this record ?')) {
    this.service.deleteSale($key);
    this.notificationService.warn('! Deleted successfully');
    }
  }



}
