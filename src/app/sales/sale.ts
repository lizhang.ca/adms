export class Sale {
  id: number;
  firstName: string;
  lastName: string;
  mobile: number;
  email: string;
  gender: string;
}
