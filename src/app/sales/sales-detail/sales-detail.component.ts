import { CustomerService } from './../../shared/customer.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Sale } from '../sale';
import { SalesService } from '../../shared/sales.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DepartmentService } from 'src/app/shared/department.service';
import { NotificationService } from 'src/app/shared/notification.service';
import { MatDialogRef } from '@angular/material';
import { CarService } from 'src/app/shared/car.service';

@Component({
  selector: 'app-sales-detail',
  templateUrl: './sales-detail.component.html',
  styleUrls: ['./sales-detail.component.css']
})
export class SalesDetailComponent implements OnInit {
  sale$: Observable<Sale>;
  alerts: string;

  constructor(
    public service: SalesService,
    private notificationService: NotificationService,
    public carService: CarService,
    private router: Router,
    // public dialogRef: MatDialogRef<SalesDetailComponent>
    ) { }

  ngOnInit() {
    this.service.getSales();
   this.carService.getCars();
  }

  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
  }

  onSubmit() {
    if (this.service.form.valid) {
      if (!this.service.form.get('$key').value) {
        this.service.insertSale(this.service.form.value);
      } else {
        this.service.updateSale(this.service.form.value);
      }
      // this.service.form.reset();
      // this.service.initializeFormGroup();
      this.notificationService.success('::Submitted successfully');
      // this.onClose();
      const that = this;
      setTimeout(() => { that.router.navigate(['/sales']); }, 1000);

    }
  }

  onClose() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    // this.dialogRef.close();
  }

  onBack() {
    this.router.navigate(['/sales']);
  }
}
