import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesListComponent } from './sales-list/sales-list.component';
import { AuthGuard } from '../shared/auth.guard';
import { SalesDetailComponent } from './sales-detail/sales-detail.component';


const salesRoutes: Routes = [
  { path: 'sales', component: SalesListComponent  ,  canActivate: [AuthGuard]},
  { path: 'sale/:id', component: SalesDetailComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(salesRoutes)
  ],
  exports: [
    RouterModule
  ]

})
export class SalesRoutingModule { }
