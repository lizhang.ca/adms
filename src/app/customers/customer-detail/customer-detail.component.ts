import { DepartmentService } from '../../shared/department.service';
import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { Observable } from 'rxjs';
import { CustomerService } from '../../shared/customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'src/app/shared/notification.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {
  customer$: Observable<Customer>;
  alerts: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public service: CustomerService,
    public departmentService: DepartmentService,
    private notificationService: NotificationService,
    // public dialogRef: MatDialogRef<CustomerDetailComponent>
    ) { }

  ngOnInit() {
    this.service.getCustomers();
  }

  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    // this.notificationService.success('::Submitted successfully');

  }

  onSubmit() {
    if (this.service.form.valid) {
      if (!this.service.form.get('$key').value) {
        this.service.insertCustomer(this.service.form.value);
      } else {
        this.service.updateCustomer(this.service.form.value);
      }
      // this.service.form.reset();
      // this.service.initializeFormGroup();
      this.notificationService.success('::Submitted successfully');
      // this.onClose();
      const that = this;
      setTimeout(() => { that.router.navigate(['/customers']); }, 1000);

    }
  }

  onClose() {
    this.service.form.reset();
    this.service.initializeFormGroup();
    // this.dialogRef.close();
  }

  onBack() {
    this.router.navigate(['/customers']);
  }
}
