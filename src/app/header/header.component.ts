import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { User, AuthService } from '../shared/auth.service';
import { Store } from '../shared/store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() user: User;
  @Output() logout: EventEmitter<any> = new EventEmitter<any>();
  @Output() sideToggle: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  logoutUser() {
    this.logout.emit();
  }

  navToggle() {
    this.sideToggle.emit();
  }
}
