import { HttpClientModule } from '@angular/common/http';
import { Store } from './shared/store';
import { environment } from './../environments/environment';
import { MaterialModule } from './material';
import { AuthModule } from './auth/auth.module';
import { SalesModule } from './sales/sales.module';
import { CarsModule } from './cars/cars.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { AngularFireModule } from 'angularfire2';
// import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
// import { FlexLayoutModule } from '@angular/flex-layout';

import { DatePipe } from '@angular/common';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { CustomersModule } from './customers/customers.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { CustomerDetailComponent } from './customers/customer-detail/customer-detail.component';
import { CarDetailComponent } from './cars/car-detail/car-detail.component';
import { SalesDetailComponent } from './sales/sales-detail/sales-detail.component';
import { HeaderComponent } from './header/header.component';
import { UploadComponent } from './upload/upload/upload.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    HeaderComponent,
    UploadComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    CarsModule,
    SalesModule,
    CustomersModule,
    DashboardModule,
    AuthModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    HttpClientModule,
  //  FlexLayoutModule
  ],
  providers: [DatePipe, Store],
  bootstrap: [AppComponent],
  entryComponents: [CustomerDetailComponent, CarDetailComponent, SalesDetailComponent],
})
export class AppModule { }
