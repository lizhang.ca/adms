import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { User, AuthService } from './shared/auth.service';
import { Store } from './shared/store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'adms';


  user$: Observable<User>;
  subscriptionAuth: Subscription;

  constructor(private store: Store, public authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.subscriptionAuth = this.authService.auth$.subscribe();
    this.user$ = this.store.select<User>('user');
  }

  ngOnDestroy() {
    this.subscriptionAuth.unsubscribe();
  }

  async logoutUser() {
    await this.authService.logoutUser();
    this.router.navigate(['/login']);
  }

}
