import { ColorService } from 'src/app/shared/color.service';
import { switchMap, finalize } from 'rxjs/operators';
import { CarService } from '../../shared/car.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Car } from '../car';
import { NotificationService } from 'src/app/shared/notification.service';
import { MatDialogRef } from '@angular/material';
import { AngularFireObject, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css']
})
export class CarDetailComponent implements OnInit {
  car$: Observable<Car>;
  alerts: string;

  // photoRef: AngularFireObject<any>;
  // photos: Observable<any>;
  // uploadPercent: Observable<number>;
  // photoDownloadURL: Observable<string>;


  constructor(
    private db: AngularFireDatabase,
    private storage: AngularFireStorage,

    private route: ActivatedRoute,
    private router: Router,
    public service: CarService,
    private notificationService: NotificationService,
   // public dialogRef: MatDialogRef<CarDetailComponent>,
    public colorService: ColorService
  ) {
    // this.photoRef = db.object('photos/featured');
    // this.photos = this.photoRef.valueChanges();

  }


  ngOnInit() {
    this.service.getCars();

  }

  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
  }

  onSubmit() {
    if (this.service.form.valid) {
      if (!this.service.form.get('$key').value) {
        this.service.insertCar(this.service.form.value);
      } else {
        this.service.updateCar(this.service.form.value);
      }
      // this.service.form.reset();
      // this.service.initializeFormGroup();
      this.notificationService.success('::Submitted successfully');
      // this.onClose();
      const that = this;
      setTimeout(() => { that.router.navigate(['/cars']); }, 1000);

    }
  }

  onClose() {
    this.service.form.reset();
    this.service.initializeFormGroup();
  //  this.dialogRef.close();
  }

  onBack() {
    this.router.navigate(['/cars']);
  }

//   uploadPhoto(event: any) {
//     const file = event.target.files[0];
//     const filePath = '/photos/featured/url1';
//     const fileRef = this.storage.ref(filePath);
// //    const task = fileRef.put(file);
//     const task = this.storage.upload(filePath, file);
//     console.log('Uploading:...' , file.name);

//     this.uploadPercent = task.percentageChanges();
//     task.snapshotChanges().pipe(
//       finalize(() => {
//         console.log('Upload finished!');
//         this.photoDownloadURL = fileRef.getDownloadURL();
//         this.updatePhotoUrl();
//       })).subscribe();
//   }

//   updatePhotoUrl(): any {
//     this.photoDownloadURL.subscribe(obj => {
//       this.photoRef.set({ url1: obj});
//       this.service.form.get('images').setValue(obj);
//     });
//   }
}
