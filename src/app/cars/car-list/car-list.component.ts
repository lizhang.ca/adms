import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap, tap, map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { Car } from '../car';
import { CarService } from '../../shared/car.service';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogConfig } from '@angular/material';
import { NotificationService } from 'src/app/shared/notification.service';
import { CarDetailComponent } from '../car-detail/car-detail.component';
import { ColorService } from 'src/app/shared/color.service';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit {

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'year', 'brand', 'price', 'colorName', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;

  constructor(private service: CarService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private colorService: ColorService) {}

  ngOnInit() {
    this.service.getCars().subscribe(
      list => {
        const array  = list.map(item => {
          const colorName = this.colorService.getColorName(item.payload.val()['color']);
          return {
            $key: item.key,
            colorName: colorName,
            ...item.payload.val()
          };
        });
        this.listData = new MatTableDataSource(array);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele !== 'actions' && data[ele].toLowerCase().indexOf(filter) !== -1;
          });
        };
      });
    }


  onSearchClear() {
    this.searchKey = '';
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLocaleLowerCase();
  }


  onCreate () {
    this.service.initializeFormGroup();
    this.router.navigate(['/car/new']);
    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.width = '60%';
    // this.dialog.open(CarDetailComponent, dialogConfig);
  }


  onEdit(row) {
    this.service.populateForm(row);
    this.router.navigate(['/car/' + row.$key]);

    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.width = '60%';
    // this.dialog.open(CarDetailComponent, dialogConfig);
  }

  onDelete($key) {
    if (confirm('Are you sure to delete this record ?')) {
    this.service.deleteCar($key);
    this.notificationService.warn('! Deleted successfully');
    }
  }

}
