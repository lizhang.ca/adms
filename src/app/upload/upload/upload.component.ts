import { Component } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireObject } from '@angular/fire/database';
// import * as firebase from 'firebase';

import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent {
  photoRef: AngularFireObject<any>;
  photos: Observable<any>;
  audioRef: AngularFireObject<any>;
  audios: Observable<any>;
  videoRef: AngularFireObject<any>;
  videos: Observable<any>;

  uploadPercent: Observable<number>;
  photoDownloadURL: Observable<string>;
  audioDownloadURL: Observable<string>;
  videoDownloadURL: Observable<string>;

  constructor(
    private db: AngularFireDatabase, private storage: AngularFireStorage) {
    this.photoRef = db.object('photos/featured');
    this.photos = this.photoRef.valueChanges();

    this.audioRef = db.object('audios/featured');
    this.audios = this.audioRef.valueChanges();

    this.videoRef = db.object('videos/featured');
    this.videos = this.videoRef.valueChanges();
  }

  uploadPhoto(event: any) {
    const file = event.target.files[0];
    const filePath = '/photos/featured/url1';
    const fileRef = this.storage.ref(filePath);
//    const task = fileRef.put(file);
    const task = this.storage.upload(filePath, file);
    console.log('Uploading:...' , file.name);

    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
      finalize(() => {
        console.log('Upload finished!');
        this.photoDownloadURL = fileRef.getDownloadURL();
        this.updatePhotoUrl();
      })).subscribe();
  }
  updatePhotoUrl(): any {
    this.photoDownloadURL.subscribe(obj => {
      this.photoRef.set({ url1: obj});
    });
  }

  uploadAudio(event: any) {
    const afile = event.target.files[0];
    const afilePath = '/audios/featured/url1';
    const afileRef = this.storage.ref(afilePath);
    const ameta = {'contentType': afile.type};
//    const atask = afileRef.put(afile, ameta);
    const atask = this.storage.upload(afilePath, afile);

    console.log('Uploading:...' , afile.name);

    this.uploadPercent = atask.percentageChanges();
    atask.snapshotChanges().pipe(
      finalize(() => {
        console.log('Upload finished!');
        this.audioDownloadURL = afileRef.getDownloadURL();
        this.updateAudioUrl();
      })).subscribe();
  }
  updateAudioUrl(): any {
    this.audioDownloadURL.subscribe(obj => {
      this.audioRef.set({ url1: obj});
    });
  }


  uploadVideo(event: any) {
    const vfile = event.target.files[0];
    const vfilePath = '/videos/featured/url1';
    const vfileRef = this.storage.ref(vfilePath);
    const vMetaData = {'contentType': vfile.type};
    // console.log(vfile.type);
    // const vtask = vfileRef.put(vfile, vMetaData);
    const vtask = this.storage.upload(vfilePath, vfile);
    console.log('Uploading:...' , vfile.name);

    this.uploadPercent = vtask.percentageChanges();
    vtask.snapshotChanges().pipe(
      finalize(() => {
        console.log('Upload finished!');
        this.videoDownloadURL = vfileRef.getDownloadURL();
        this.updateVideoUrl();
      })).subscribe();
  }
  updateVideoUrl(): any {
    this.videoDownloadURL.subscribe(obj => {
      console.log(obj);
      this.videoRef.set({ url1: obj});
    });
  }


}
