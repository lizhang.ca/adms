import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { DatePipe } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class CarService {

  carList: AngularFireList<any>;
  array = [];

  constructor(
    private firebase: AngularFireDatabase,
    private datePipe: DatePipe
    ) { }

  form = new FormGroup({
    $key: new FormControl(null),
    name: new FormControl('', [Validators.required]),
    // email: new FormControl('', Validators.email),
    // mobile: new FormControl('')
    year: new FormControl(''),
    brand: new FormControl(''),
    entryDate: new FormControl(''),
    booked: new FormControl('1'),
    price: new FormControl(''),
    discount: new FormControl(''),
    color: new FormControl('0'),
    trim: new FormControl(''),
    images: new FormControl('')
  });

  initializeFormGroup() {
    this.form.setValue({
      $key: null,
      name: '',
      // email:  '',
      // mobile: ''
      year: '',
      brand: '',
      entryDate: '',
      booked: '1',
      price: '',
      discount: '',
      color: 0,
      trim: '',
      images: ''
    });
  }

  populateForm(car) {
    this.form.setValue(_.omit(car, 'colorName'));
  }

  getCars() {
    this.carList = this.firebase.list('cars');
    this.array = ['X1', 'X2', 'X3'];
    return this.carList.snapshotChanges();
  }

  insertCar(car) {
    this.carList.push({
      name: car.name,
      // email: car.email,
      // mobile: car.mobile
      year: car.year,
      brand: car.brand,
      entryDate: car.entryDate === '' ? '' : this.datePipe.transform(car.entryDate, 'yyyy-MM-dd'),
      booked: car.booked,
      price: car.price,
      discount: car.discount,
      color: car.color,
      trim: car.trim,
      images: car.images
    });
  }

  updateCar(car) {
    this.carList.update(car.$key,
    {
      name: car.name,
      // email: car.email,
      // mobile: car.mobile
      year: car.year,
      brand: car.brand,
      entryDate: car.entryDate,
      price: car.price,
      discount: car.discount,
      color: car.color,
      trim: car.trim,
      images: car.images
    });
  }

  deleteCar($key) {
    this.carList.remove($key);
  }




  // getCarName($key) {

  //   if ($key === '0') {
  //     return '';
  //   } else {
  //     console.log(this.carList.query);
  //    // console.log(_.find(this.array, (obj) => {return obj.fullName === $key;} )['mobile']);
  //   //  return _.find(this.array, (obj) => {return obj.fullName === $key;} )['mobile'];
  //   }

  // }


}
