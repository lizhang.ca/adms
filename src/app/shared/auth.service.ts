import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { delay, tap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';

import { Store } from './store';
import { Router } from '@angular/router';
// import firebase = require('firebase');

export interface User {
  email: string;
  authed: boolean;
  uid: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public  isSignedInStream: Observable<boolean>;
  public displayNameStream: Observable<string>;
  photoUrl: string;

   isLoggedIn = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  auth$ = this.af.authState.pipe(
    tap(next => {
      if (!next) {
        this.store.set('user', null);
        return;
      }
      const user: User = {
        email: next.email,
        uid: next.uid,
        authed: true
      };

      this.store.set('user', user);
    })
  );

  constructor(
    private router: Router,
    private store: Store,
    private af: AngularFireAuth
  ) {
    this.af.authState.subscribe((user: firebase.User) => {
      if (user) {
        console.log('user is signed as ', user.displayName);
        this.photoUrl = user.photoURL;
       } else {
        console.log('user is not signed in');
        this.photoUrl = '';
       }
    });

    this.isSignedInStream = this.af.authState
    .pipe(
      map((user: firebase.User)  => {
        return user != null;
      }));

    this.displayNameStream = this.af.authState
    .pipe(
      map((user: firebase.User)  => {
        if (user != null) {
          return user.displayName;
        }
        return '';
      }));
  }


  signInWithGoogle () {
    this.af.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((user) => {
        this.router.navigate(['/']);
      });
  }

  signOutWithGoogle () {
    console.log('signout with google.');
    this.af.auth.signOut();
    location.reload();
    // this.router.navigate(['/dashboard']);
  }

  // login(): Observable<boolean> {
  //   return of(true).pipe(
  //     delay(1000),
  //     tap(val => this.isLoggedIn = true)
  //   );
  // }

  // logout(): void {
  //   this.isLoggedIn = false;
  // }


  loginUser(email: string, password: string) {
    return this.af.auth.signInWithEmailAndPassword(email, password);
  }

  logoutUser() {
    return this.af.auth.signOut();
  }

  createUser(email: string, password: string) {
    return this.af.auth.createUserWithEmailAndPassword(email, password);
  }
}
