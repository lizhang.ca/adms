import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  colorList: AngularFireList<any>;
  array = [];

  constructor(private firebase: AngularFireDatabase) {
    this.colorList = this.firebase.list('color');
    this.colorList.snapshotChanges().subscribe(
      list => {
        this.array = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });
  }

  getColorName($key) {
    if ($key === '0') {
      return '';
    } else {
      return _.find(this.array, (obj) => {return obj.$key === $key;} )['value'];
    }

  }

}
