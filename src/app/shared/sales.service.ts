import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class SalesService {
  saleList: AngularFireList<any>;

  constructor(
    private firebase: AngularFireDatabase) {}

    form = new FormGroup({
      $key: new FormControl(null),
      car: new FormControl('', Validators.required),
      customers: new FormControl('')
    });

    initializeFormGroup() {
      this.form.setValue({
        $key: null,
        car: '',
        customers: ''
      });
    }

    populateForm(sale) {
      //this.form.setValue(sale);
      this.form.setValue(_.omit(sale, 'detail'));

    }

    getSales() {
      this.saleList = this.firebase.list('sales');
      return this.saleList.snapshotChanges();
    }

    insertSale(sale) {
      this.saleList.push({
        car: sale.car,
        customers: sale.customers,
       });
    }

  updateSale(sale) {
    this.saleList.update(sale.$key, {
      car: sale.car,
      customers: sale.customers
    });
  }

  deleteSale($key) {
    this.saleList.remove($key);
  }
}
