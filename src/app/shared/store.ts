import { User } from './auth.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, pluck } from 'rxjs/operators';

export interface State {
  user: User;
  [key: string]: any;
}

const state: State = {
  user: undefined
};

export class Store {
  private subject$ = new BehaviorSubject<State>(state);
  private store = this.subject$.asObservable().pipe(distinctUntilChanged());

  get value() {
    return this.subject$.value;
  }

  set(key: string, value: any) {
    this.subject$.next({...this.value, [key]: value});
  }

  select<T>(key: string): Observable<T> {
    return this.store.pipe(pluck(key));
  }
}
