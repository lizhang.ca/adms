import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  customerList: AngularFireList<any>;

  constructor(private firebase: AngularFireDatabase, private datePipe: DatePipe) {
  }

  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    fullName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    mobile: new FormControl('', [Validators.required, Validators.minLength(8)]),
    city: new FormControl(''),
    gender: new FormControl('1'),
    department: new FormControl('0'),
    hireDate: new FormControl(''),
    isPermanent: new FormControl(false)
  });

  initializeFormGroup() {
    this.form.setValue({
      $key: null,
      fullName: '',
      email:  '',
      mobile: '',
      city:  '',
      gender:  '1',
      department: 0,
      hireDate:  '',
      isPermanent: false
    });
  }

  populateForm(customer) {
    this.form.setValue(_.omit(customer, 'departmentName'));
  }

  getCustomers() {
    this.customerList = this.firebase.list('customers');
    return this.customerList.snapshotChanges();
  }

  insertCustomer(customer) {
    this.customerList.push({
      fullName: customer.fullName,
      email: customer.email,
      mobile: customer.mobile,
      city: customer.city,
      gender: customer.gender,
      department: customer.department,
      hireDate: customer.hireDate === '' ? '' : this.datePipe.transform(customer.hireDate, 'yyyy-MM-dd'),
      isPermanent: customer.isPermanent
    });
  }

  updateCustomer(customer) {
    this.customerList.update(customer.$key, {
      fullName: customer.fullName,
      email: customer.email,
      mobile: customer.mobile,
      city: customer.city,
      gender: customer.gender,
      department: customer.department,
      hireDate: customer.hireDate,
      isPermanent: customer.isPermanent
    });
  }

  deleteCustomer($key: string) {
    this.customerList.remove($key);
  }


  getCustomerDetails($key: string) {
    if ($key === '') {
      return '';
    } else {
      return  {
        $key: 'fake-key',
        fullName: 'zhangsan',
        email:  'z@san.com',
        mobile: '12345678',
        city:  'toronto',
        gender:  '1',
        department: 'abc',
        hireDate:  '2018-1-1',
        isPermanent: false
      } ;
    }

  }


  // getCustomerName($key) {

  //   if ($key === '0') {
  //     return '';
  //   } else {
  //     console.log(this.array);
  //    // console.log(_.find(this.array, (obj) => {return obj.fullName === $key;} )['mobile']);
  //   //  return _.find(this.array, (obj) => {return obj.fullName === $key;} )['mobile'];
  //   }

  // }


}
