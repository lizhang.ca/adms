import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { Router, NavigationExtras } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  message: string;
  error: string;

  constructor(public authService: AuthService, public router: Router) {
    // this.setMessage();
  }

  // setMessage() {
  //   this.message = 'Logged ' + (this.authService.isLoggedIn ? 'in' : 'out');
  // }

  ngOnInit() {
  }

  // async loginUser(fm: FormGroup) {
  //   const {email, password} = fm.value;
  //   try {
  //     await this.authService.loginUser(email, password);
  //     this.router.navigate(['/admin']);
  //   } catch (e) {
  //     this.error = e.message;
  //   }
  // }

  // async loginUser(fm: FormGroup) {
  //   const {email, password} = fm.value;
  //   try {
  //     await this.authService.loginUser(email, password);
  //     this.authService.isLoggedIn = true;
  //     this.router.navigate(['/dashboard']);
  //   } catch (e) {
  //     this.error = e.message;
  //   }
  // }

  // async logoutUser() {
  //   await this.authService.logoutUser();
  //   this.authService.isLoggedIn = false;
  //   this.router.navigate(['/dashboard']);
  // }

  // login() {
  //   this.message = 'Trying to log in ...';

  //   this.authService.login().subscribe(() => {
  //     this.setMessage();
  //     if (this.authService.isLoggedIn) {
  //       const redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/admin';

  //       const navigationExtras: NavigationExtras = {
  //         queryParamsHandling: 'preserve',
  //         preserveFragment: true
  //       };

  //       // Redirect the user
  //       this.router.navigate([redirect], navigationExtras);
  //     }
  //   });
  // }

  // logout() {
  //   this.authService.logout();
  //   this.setMessage();
  // }

}
