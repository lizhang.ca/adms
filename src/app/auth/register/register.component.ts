import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  error: string;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  async registerUser(fm: FormGroup) {
    // console.log(fm);
    const {email, password} = fm.value;
    try {
      await this.authService.createUser(email, password);
      this.router.navigate(['/dashboard']);
    } catch (e) {
      this.error = e.message;
    }
  }
}
